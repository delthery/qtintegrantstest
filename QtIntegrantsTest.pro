#-------------------------------------------------
#
# Project created by QtCreator 2016-06-23T10:55:55
#
#-------------------------------------------------

QT       += core
QT       += testlib
QT       -= gui

TARGET = QtIntegrantsTest
CONFIG   += console
CONFIG   -= app_bundle
#CONFIG   += warn_off
CONFIG += c++11

TEMPLATE = app

#CONFIG(release){
#    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
#}
#else {
#    DEFINES += DEBUG
#}

SOURCES += main.cpp \
    unit.cpp \
    sharedunit.cpp \
    thread.cpp

HEADERS += \
    unit.h \
    sharedunit.h \
    thread.h
