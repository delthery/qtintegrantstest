#include <QCoreApplication>
#include <QVector>
#include <QDebug>
#include <QTest>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include "unit.h"
#include "sharedunit.h"
#include "thread.h"

void UnitPushThread(Unit* unit)
{
    //QByteArray data = QByteArray::fromHex("010000020000");
    QByteArray data = QByteArray::fromHex("010102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A3B2C2D2E2F303132333435363738393A3B3C3D3E3F");
    QThread::sleep(1);
    for(int i = 0; i < 10; i++) {
        qDebug() << "UnitPushThread: pushing" << i+1;
         unit->Push(data);
         //QThread::sleep(1);
         QThread::usleep(200);
    }
}

void SharedUnitPushThread(SharedUnit* unit)
{
    QByteArray data = QByteArray::fromHex("0100000200");
    QThread::sleep(1);
    for(int i = 0; i < 10; i++) {
        qDebug() << "SharedUnitPushThread: pushing" << i+1;
         unit->Push(data);
         QThread::sleep(1);
         //QThread::usleep(200);
    }
}

class UnitTest : public QObject
{
    Q_OBJECT
private slots:

//    void Do()
//    {
//        QByteArray data1 = QByteArray::fromHex("0100000200");
//        //QByteArray data1 = QByteArray::fromHex("0100");
//        //QByteArray data1 = QByteArray::fromHex("");
//        QByteArray data2 = QByteArray::fromHex("020000010101");
//        QByteArray data3 = QByteArray::fromHex("030000020201");
//        QByteArray data4 = QByteArray::fromHex("040000030201");

//        Unit u;
//        Integrant i1("i1", 1);
//        Integrant i2("i2", 2);
//        Integrant i3("i3", 3, 4);
//        Integrant i4("i4", 4, 6);

//        u.Add(&i1);
//        u.Add(&i2);
//        u.Add(&i3);

//        qDebug() << "===========================Test1===========================" << data1.toHex();
//        u.Job(data1);
//        qDebug() << "===========================Test2===========================" << data2.toHex();
//        u.Job(data2);
//        qDebug() << "===========================Test3===========================" << data3.toHex();
//        u.Job(data3);
//        qDebug() << "===========================Test4===========================" << data4.toHex();
//        u.Job(data4);
//        u.Add(&i4);
//        u.Job(data4);
//    }

//    void Bench()
//    {
//        const int elementaryMessageLength = 64;
//        const int integrantsNumber = 24;
//        int messageLength = integrantsNumber * elementaryMessageLength;
//        QString hexString;

////        qsrand(QTime::currentTime().msec());
////        for(int i = 0 ; i < messageLength / sizeof(int); i++)
////            hexString.append(QString::number(qrand(), 16));

////        QByteArray local = hexString.toLatin1();//toLocal8Bit();
////        QByteArray data = QByteArray::fromHex(local);

//        Unit u;
//        for(int i = 0; i < integrantsNumber; i++)
//        {
//            Integrant* igrant = new Integrant(QString::number(i+1), i+1, elementaryMessageLength);
//            u.Add(igrant);
//            for(int j = 0 ; j < messageLength / (integrantsNumber*elementaryMessageLength); j++)
//            {
//                for(int k = 0; k < elementaryMessageLength; k++)
//                    hexString.append(QString::number(igrant->Sign()));
//            }
//        }
//        QByteArray data = hexString.toLocal8Bit();

//        QBENCHMARK {
//            u.Job(data);
//        }
//    }

//    void QueuePushTest()
//    {
//        Queue queue;
//        QByteArray data = QByteArray::fromHex("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A3B2C2D2E2F303132333435363738393A3B3C3D3E3F");

//        QBENCHMARK{
//            queue.Push(data);
//        }
//    }

//    void QueueRemoveTest()
//    {
//        Queue queue;
//        QByteArray data = QByteArray::fromHex("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A3B2C2D2E2F303132333435363738393A3B3C3D3E3F");

//        queue.Push(data);
//        queue.Push(data);

//        QBENCHMARK{
//            queue.Push(data);
//            queue.Remove(1);
//        }
//    }

//    void QueuedUnitTest()
//    {
//        //QByteArray data = QByteArray::fromHex("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A3B2C2D2E2F303132333435363738393A3B3C3D3E3F");
//        QByteArray data = QByteArray::fromHex("0100000200");
//        Unit u;
//        Integrant i1("i1", 1);
//        Integrant i2("i2", 2);
//        Integrant i3("i3", 3, 4);
//        Integrant i4("i4", 4, 6);

//        u.Add(&i1);
//        u.Add(&i2);
//        u.Add(&i3);

//        u.Push(data);
//    }

//    void AddToNotEmptyUnitTest()
//    {
//        QByteArray data = QByteArray::fromHex("0100000200");
//        Unit u;
//        Integrant i1("i1", 1);
//        connect(&u, &Unit::UnknownMessageNotify, [=](QByteArray& data) {
//            qDebug() << "Unknown message:" << data.toHex();
//        });

//        // push before adding integrant
//        u.Push(data);
//        // added integrant must try to process unit data
//        u.Add(&i1);
//    }

//    void UnknownMessageUnitTest()
//    {
//        QByteArray data = QByteArray::fromHex("0100000200");
//        Unit u;
//        //unit without integrants, all message is unknown

//        connect(&u, &Unit::UnknownMessageNotify, [=](QByteArray& data) {
//            qDebug() << "Unknown message:" << data.toHex();
//        });

//        u.Push(data);
//    }
/*
    void MultithreadedUnitTest()
    {
        Unit u;
        Integrant i1("i1", 1);
        Integrant i2("i2", 2);

        u.Add(&i1);
        u.Add(&i2);

        QFuture<void> future = QtConcurrent::run(UnitPushThread, &u);

        //future.waitForFinished();
        for(;;) {
            qApp->processEvents();
            if(future.isFinished())
                return;
        }
    }
*/
//    void MultithreadedSharedUnitTest()
//    {
//        SharedUnit u;
//        SharedIntegrant i1("i1", 1);

//        u.Add(&i1);

//        QFuture<void> future = QtConcurrent::run(SharedUnitPushThread, &u);

//        for(;;){
//            qApp->processEvents();
////NOTE debug mode
////            if(future.isFinished())
////                return;
//        }
//    }

    void SessionTest()
    {
        Device d(4196, 4);
        Session s(&d);

        s.StartThreads();

        QThread::sleep(2);

        s.StopThreads();
    }

};
QTEST_MAIN(UnitTest)
#include "main.moc"
