#include "sharedunit.h"


SharedIntegrant::SharedIntegrant(QString name, int sign, int length) :
    QObject(),
    _name(name),
    _sign(sign),
    _length(length)
{
}

// check message type
bool SharedIntegrant::Token(QByteArray &data)
{
    if(data.startsWith(_sign)) {
        qDebug() << "\t" << _name << _sign << data.toHex() << "ok";
        return true;
    }
    qDebug() << "\t" << _name << _sign << data.toHex();
    return false;
}

void SharedIntegrant::Do(SharedQueue data)
{
    if(data.Stream().atEnd()) return;

    QByteArray array = data.Stream().device()->peek(1);
    if(!Token(array)) return;

    int readed = data.Stream().skipRawData(_length);
    qDebug() << "\treaded" << readed
             << "bytes from" << _length
             << ((readed < _length) ?
                     "unexpected message length" :
                     "ok");

    if(readed < _length){
        qDebug() << "\trewind" << readed << "bytes";
        data.Stream().device()->seek(data.Stream().device()->pos() - readed);
    }
}


///
/// \brief SharedUnit::Add
/// \param i
///
SharedUnit::SharedUnit()
{
    //connect(this, &Do, this, &Job, Qt::QueuedConnection);
    connect(this, &SharedUnit::Pushing, this, &SharedUnit::Job, Qt::QueuedConnection);
}

void SharedUnit::Add(SharedIntegrant *i)
{
    // TODO lock adding while processing
    _integrants.push_back(i);
    //connect(this, &SharedUnit::Do, i, &SharedIntegrant::Do, Qt::QueuedConnection);

    // integrants invoked in processing thread
    connect(this, &SharedUnit::Do, i, &SharedIntegrant::Do, Qt::DirectConnection);

    qDebug() << "new integrant added" << i->Name();

    if(_queue.Stream().device()->size() > 0) //TODO connection to the not empty unit
        // Will automatically change threads, if needed, to execute
        //QMetaObject::invokeMethod(i, "Do", Q_ARG(QDataStream&, _stream));
        QMetaObject::invokeMethod(i, "Do", Q_ARG(SharedQueue&, _queue));
}

// pushing thread
void SharedUnit::Push(QByteArray data)
{
    emit Pushing(data);
    // _queue.Push(data);
    // emit Job(_queue);
}

// processing thread
void SharedUnit::Job(QByteArray data)
{
    _queue.Push(data);

    int loop = 0;
    bool consumed = false;
    do {
        qDebug() << "Processing" << ++loop << "loop:";
        auto pos = _queue.Stream().device()->pos();
        emit Do(_queue);
        if(_queue.Stream().device()->pos() == pos) {
            qDebug() << "unknown message type";
            emit UnknownMessageNotify(
                        _queue.Stream().device()->peek(
                        _queue.Stream().device()->size()));
            break;
        }
        else consumed = true;
    } while(!_queue.Stream().atEnd());
}
//void SharedUnit::Job(SharedQueue queue)
//{
//    int loop = 0;
//    do {
//        qDebug() << "Processing" << ++loop << "loop:";
//        auto pos = queue.Stream().device()->pos();
//        emit Do(queue);
//        if(queue.Stream().device()->pos() == pos) {
//            qDebug() << "unknown message type";
//            emit UnknownMessageNotify(
//                        queue.Stream().device()->peek(
//                        queue.Stream().device()->size()));
//            break;
//        }
//    } while(!queue.Stream().atEnd());
//}
