#ifndef SHAREDUNIT
#define SHAREDUNIT
#include <QObject>
#include <QVector>
#include <QByteArray>
#include <QDataStream>
#include <QQueue>
#include <QMutexLocker>

#include <QDebug>


class SharedIntegrant;

///
/// \brief The SharedQueueData class
///
class SharedQueueData : public QSharedData
{
public:
    SharedQueueData(): _stream(_data) {}
    SharedQueueData(const SharedQueueData &other)
        : QSharedData(other),
          _data(other._data),
          _messages(other._messages) {
        qDebug() << QStringLiteral("SharedQueueData copy constructor");
    }
    ~SharedQueueData() {}

    struct Message {
        int pos; //< position of the first byte
        int len; //< message length
        Message(int p = 0, int l = 0):
            pos(p), len(l) {}
    };

    void Push(QByteArray data) {
        QMutexLocker locker(&_mutex);
        _messages.push_back(Message(_data.length(), data.length()));
        _data.append(data);
        //emit Process(_stream);
    }

    QByteArray Pop(int index) {
        QMutexLocker locker(&_mutex);
        return _data.mid(_messages[index].pos, _messages[index].len);
    }

    QByteArray Pop() { return Pop(_messages.length()); }

    void Remove(int index) {
        QMutexLocker locker(&_mutex);
        _data.remove(_messages[index].pos, _messages[index].len);
        for(auto i = index + 1; i < _messages.length(); i++) {
            _messages[i].pos -= _messages.at(index).len;
        }
        _messages.remove(index);
    }

    QDataStream& Stream(){
        return _stream;
    }

    QMutex _mutex;
    QByteArray _data;
    QDataStream _stream;
    QVector<Message> _messages;
};



///
/// \brief The SharedQueue class
///
class SharedQueue// : public QObject
{
    //Q_OBJECT

public:
    SharedQueue() { _instance = new SharedQueueData; }
    SharedQueue(const SharedQueue &other) : _instance(other._instance) {
        qDebug() << QStringLiteral("SharedQueue copy constructor");
    }

    void Push(QByteArray data) {
        _instance->Push(data);
        //emit Process(this);
    }
    QByteArray Pop(int index) { return _instance->Pop(index); }
    void Remove(int index) { _instance->Remove(index); }

    QDataStream& Stream(){ return _instance->Stream(); }

//signals:
//    void Process(const SharedQueue& queue);

private:
    QSharedDataPointer<SharedQueueData> _instance;
};


///
/// \brief The Integrant class
///
class SharedIntegrant : public QObject
{
    Q_OBJECT
public:
    SharedIntegrant(QString name, int sign, int length = 3);

    QString Name() const {return _name;}
    int Sign() const {return _sign;}
    int Length() const {return _length;}

private:
    QString _name;
    int _sign;
    int _length;

public slots:
    bool Token(QByteArray& data);
    void Do(SharedQueue data);
};



class SharedUnit : public QObject
{
    Q_OBJECT
public:
    SharedUnit();
    void Add(SharedIntegrant *i);
    //void Job(SharedQueue queue);
    void Job(QByteArray data);

    void Push(QByteArray data);
signals:
    void Do(SharedQueue &queue);
    //void Do(const QDataStream& data);
    void Pushing(QByteArray data);

    void UnknownMessageNotify(QByteArray& data);
private:
    QVector<SharedIntegrant*> _integrants;
    SharedQueue _queue;
//public slots:
//    void Processing(const QDataStream &stream);
};

#endif // SHAREDUNIT

