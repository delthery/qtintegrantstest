#ifndef THREAD_H
#define THREAD_H

#include <QObject>
#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QDebug>

//#include <cvmarkersobj.h>

//using namespace Concurrency::diagnostic;


class Session;

class Device
{
public:
	struct Chunk
	{
		Q_DISABLE_COPY(Chunk)
	public:
		bool empty;
		QMutex mutex;
		QWaitCondition notEmpty;
		QWaitCondition notFull;
		QByteArray* data;		

		Chunk(QByteArray* d) : data(d) {}
		~Chunk() { delete data; }
	};

	Device(int chunkSize, int depth) :
		_chunkSize(chunkSize),
		_depth(depth)
	{		
		for (int i = 0; i < depth; i++) {
			QByteArray* chunk = new QByteArray;
			chunk->reserve(chunkSize);
			_chunks.push_back(new Chunk(chunk));
		}
		_wrPtr = 0;
		_rdPtr = 0;
		qDebug() << "Device::ctor" << QThread::currentThreadId();
	}
	QByteArray Read()
	{
		_chunks[_rdPtr]->mutex.lock();
		if (_chunks[_rdPtr]->empty)
			_chunks[_rdPtr]->notEmpty.wait(&_chunks[_rdPtr]->mutex);
		QByteArray result = _chunks[_rdPtr]->data->left(_chunks[_rdPtr]->data->length());		
		_chunks[_rdPtr]->data->clear(); //NOTE is data clearing after 'left' necessary? Yes it is!		
		_chunks[_rdPtr]->empty = true;
		_chunks[_rdPtr]->notFull.wakeOne();
		_chunks[_rdPtr]->mutex.unlock();		
		_rdPtr = (_rdPtr + 1) % _depth;
		qDebug() << "Device::Read" << result.toHex() << "id:" << QThread::currentThreadId();
		return result;
	}
	void Write(QByteArray data)
	{
		qDebug() << "Device::Write" << data.toHex() << "id:" << QThread::currentThreadId();
		_chunks[_wrPtr]->mutex.lock();
		if (!_chunks[_wrPtr]->empty)
			_chunks[_wrPtr]->notFull.wait(&_chunks[_rdPtr]->mutex);
		_chunks[_wrPtr]->data->append(data);
		_chunks[_wrPtr]->empty = false;
		_chunks[_wrPtr]->notEmpty.wakeOne();
		_chunks[_wrPtr]->mutex.unlock();
		_wrPtr = (_wrPtr + 1) % _depth;		
	}


private:

	QVector<Chunk*> _chunks;
	QAtomicInt _wrPtr;
	QAtomicInt _rdPtr;
	int _chunkSize;
	int _depth;
};


class Process : public QObject
{
	Q_OBJECT

public:
	/*explicit*/ Process(Device *device = Q_NULLPTR, Session* session = Q_NULLPTR, QObject *parent = 0)
		: QObject(parent),
		  _device(device),
		  _session(session)
	{
		_wake = new QWaitCondition();
	}
protected:
	Device* _device;
	Session* _session;
	QWaitCondition* _wait;
	QWaitCondition* _wake;

	virtual void processing()
	{
		qDebug() << "Process::processing";
	}
	bool _stoped;

public slots:
    void run()
	{
		_stoped = false;

        forever {
            if(QThread::currentThread()->isInterruptionRequested()) {
                qDebug() << "Process : interruption request";
                break;
            }
            processing();
        }
        qDebug() << "Process::finished emitted";
		emit finished();
	}

	void stop()
	{
		_stoped = true;
	}

signals:
	void finished();
};

class InputProcess : public Process
{
	Q_OBJECT
public:
	Q_INVOKABLE InputProcess(Device *device = Q_NULLPTR, Session* session = Q_NULLPTR, QObject *parent = 0)
		: Process(device, session, parent)
	{
	}
	void processing()
    {
//		while(!_stoped) {
            _device->Read();
//			//QThread::usleep(10);
//		}
        //qDebug() << "InputProcess::stopped";
	}
};
//Q_DECLARE_METATYPE(InputProcess)

class OutputProcess : public Process
{
	Q_OBJECT
public:
	Q_INVOKABLE OutputProcess(Device *device = Q_NULLPTR, Session* session = Q_NULLPTR, QObject *parent = 0)
		: Process(device, session, parent)
	{
		qDebug() << "OutputProcess::ctor";
	}

	void processing()
	{
//		while(!_stoped) {
			QByteArray data = QByteArray::fromHex("010203040506");
//			data.resize(64000);
			_device->Write(data);
            QThread::usleep(20);
//		}
//		qDebug() << "OutputProcess::stopped";
	}
};
//Q_DECLARE_METATYPE(OutputProcess)




class Launcher : public QObject
{
	Q_OBJECT
public:
	Launcher(Device* device, const QMetaObject* imp, QObject *parent = 0)
		: QObject(parent),
		_imp(imp),
		_device(device)
	{
	}
public slots:
	void process()
	{
		qDebug() << "Launcher::process" << _imp->className();
		QObject* token = _imp->newInstance(
					Q_ARG(Device*, _device),
					Q_ARG(Session*, Q_NULLPTR),
					Q_ARG(QObject*, Q_NULLPTR));
		if(token) {
			_process = dynamic_cast<Process*> (token);
            if(_process) _process->run();
            else qDebug() << "Launcher::process casting failed";
		}
		else qDebug() << "Launcher::process newInstance failed";
		emit finished();
	}
	void stop()
	{
        qDebug() << "Launcher(" << _imp->className() << ")::stop";
		_process->stop();
	}
signals:
	void finished();

private:
	const QMetaObject* _imp;
	Process* _process;
	Device* _device;
};

class Session : public QObject
{
	Q_OBJECT
public:
	explicit Session(Device* device, QObject *parent = 0)
	{
		//qDebug() << "Session::ctor : InputProcess::metaObject" << InputProcess::staticMetaObject.className();
		Launcher* input = new Launcher(device, &InputProcess::staticMetaObject, parent);
		_inThread = new QThread;
		input->moveToThread(_inThread);
		connect(_inThread, &QThread::started, input, &Launcher::process);
		connect(input, &Launcher::finished, _inThread, &QThread::quit);
		connect(this, &Session::StopAll, input, &Launcher::stop);
		connect(input, &Launcher::finished, input, &Launcher::deleteLater);
		connect(_inThread, &QThread::finished, _inThread, &QThread::deleteLater);

		Launcher* output = new Launcher(device, &OutputProcess::staticMetaObject, parent);
		_outThread = new QThread;
		output->moveToThread(_outThread);
		connect(_outThread, &QThread::started, output, &Launcher::process);
		connect(output, &Launcher::finished, _outThread, &QThread::quit);
		connect(this, &Session::StopAll, output, &Launcher::stop);
		connect(output, &Launcher::finished, output, &Launcher::deleteLater);
		connect(_outThread, &QThread::finished, _outThread, &QThread::deleteLater);
//        _inProcessor = new InputProcess(device, this);
//        _inThread = new QThread;
//        _inProcessor->moveToThread(_inThread);
//        connect(_inThread, &QThread::started, _inProcessor, &Process::process);
//        connect(_inProcessor, &Process::finished, _inThread, &QThread::quit);
//        connect(this, &Session::StopAll, _inProcessor, &Process::stop);
//        connect(_inProcessor, &Process::finished, _inProcessor, &Process::deleteLater);
//        connect(_inThread, &QThread::finished, _inThread, &QThread::deleteLater);

//        _outProcessor = new OutputProcess(device, this);
//        _outThread = new QThread;
//        _outProcessor->moveToThread(_outThread);
	}

	void StartThreads()
	{
		_outThread->start();
		QThread::sleep(1);
		_inThread->start();		
	}

	void StopThreads()
	{
        qDebug() << "Session::StopThreads";
		emit StopAll();
        _inThread->requestInterruption();
        _outThread->requestInterruption();
        qDebug() << "Session::StopThreads wait";
        _inThread->wait(2);
        _outThread->wait(2);
	}

private:
	Process* _inProcessor;
	Process* _outProcessor;
	QThread* _inThread;
	QThread* _outThread;
	QMutex _mutex;

signals:
	void StopAll();
public slots:
};

#endif // THREAD_H
