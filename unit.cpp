#include "unit.h"

#include <QtTest>


Integrant::Integrant(QString name, int sign, int length) :
    QObject(),
    _name(name),
    _sign(sign),
    _length(length)
{
}

// check message type
bool Integrant::Token(QByteArray &data)
{
    if(data.startsWith(_sign)) {
        qDebug() << "\t" << _name << _sign << data.toHex() << "ok";
        return true;
    }
    qDebug() << "\t" << _name << _sign << data.toHex();
    return false;
}

void Integrant::Do(QDataStream &data)
{
    if(data.atEnd()) return;

    QByteArray array = data.device()->peek(1);
    if(!Token(array)) return;

    int readed = data.skipRawData(_length);
    qDebug() << "\treaded" << readed
             << "bytes from" << _length
             << ((readed < _length) ?
                     "unexpected message length" :
                     "ok");

    if(readed < _length){
        qDebug() << "\trewind" << readed << "bytes";
        data.device()->seek(data.device()->pos() - readed);
    }
}


Unit::Unit(int chunkSize) : QObject(),
    _chunkSize(chunkSize)
{
    // Processing chain
    // Queue::Push -> Queue::Process ---> Unit::Processing -> ( Unit::Do ---> Integrant::Do )
    //connect(/*(Queue*)*/this, &Queue::Process, this, &Unit::Processing);
    //connect(_queue, &Queue::Process, this, &Unit::Processing);

    connect(this, &Unit::Push, this, &Unit::ProcessingData, Qt::QueuedConnection);
}


void Unit::Add(Integrant *i)
{
    _integrants.push_back(i);
    connect(this, &Unit::Do, i, &Integrant::Do, Qt::DirectConnection);

    qDebug() << "new integrant added" << i->Name();

    if(_queue.Stream().device()->size() > 0) //TODO connection to the not empty unit
        // Will automatically change threads, if needed, to execute
        QMetaObject::invokeMethod(i, "Do", Q_ARG(QDataStream&, _queue.Stream()));
}


void Unit::Job(QByteArray &data)
{
    int loop = 0;
    QDataStream stream(data);

    do {
        qDebug() << "Processing" << ++loop << "loop:";
        auto pos = stream.device()->pos();
        emit Do(stream);
        if(stream.device()->pos() == pos) {
            qDebug() << "unknown message type";
            break;
        }
    } while(!stream.atEnd());
}

void Unit::Processing(QDataStream &stream)
{
    int loop = 0;
    do {
        qDebug() << "Processing" << ++loop << "loop:";
        auto pos = stream.device()->pos();
        emit Do(stream);
        if(stream.device()->pos() == pos) {
            qDebug() << "unknown message type";
            emit UnknownMessageNotify(
                        stream.device()->peek(
                            stream.device()->size()));
            break;
        }
    } while(!stream.atEnd());
}

void Unit::ProcessingData(QByteArray data)
{
    _queue.Push(data);

    if(_chunkSize > 0)
        if(_queue.Stream().device()->size() < _chunkSize)
            return;

    int loop = 0;
    do {
        qDebug() << "Processing" << ++loop << "loop:";
        auto pos = _queue.Stream().device()->pos();
        emit Do(_queue.Stream());
        if(_queue.Stream().device()->pos() == pos) {
            qDebug() << "unknown MSG type" << _queue.Stream().device()->size() << "bytes";
            emit UnknownMessageNotify(
                        _queue.Stream().device()->peek(
                        _queue.Stream().device()->size()));
            break;
        }
    } while(!_queue.Stream().atEnd());
}




