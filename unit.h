#ifndef UNIT
#define UNIT

#include <QObject>
#include <QVector>
#include <QByteArray>
#include <QDataStream>
#include <QQueue>
#include <QMutexLocker>
#include <QWaitCondition>

#include <QDebug>


///
/// \brief The Queue class
///
class Queue : public QObject
{
    Q_OBJECT

    struct Message
    {
        int pos;
        int len;
        Message(int p = 0, int l = 0):
            pos(p), len(l) {}
    };

public:
    Queue(int maxSize = 0) : QObject(),
        _stream(&_data, QIODevice::ReadWrite),
        _maxSize(maxSize)
    {
    }

    void Push(QByteArray data)
    {
        _mutex.lock();
        _messages.push_back(Message(_data.length(), data.length()));
        _data.append(data);
        _notEmpty.wakeAll();
        _mutex.unlock();

        //emit Process(_stream);
    }

    QByteArray Get(int index)
    {
        QByteArray result;

        _mutex.lock();
        if(index > _messages.length()) {
            _mutex.unlock();
            return result;
        }

//        if(_data.size() == 0)
//            _notEmpty.wait(_mutex);
        result = _data.mid(_messages[index].pos, _messages[index].len);
        _mutex.unlock();

        return result;
        //TODO warning: C4172: returning address of local variable or temporarys
    }

    QByteArray& Pull(int index)
    {
        QByteArray result = Get(index);
        Remove(index);
        return result;
        //TODO warning: C4172: returning address of local variable or temporary
    }

    void Remove(int index)
    {
        QMutexLocker locker(&_mutex);
        _data.remove(_messages[index].pos, _messages[index].len);
        for(auto i = index + 1; i < _messages.length(); i++) {
            _messages[i].pos -= _messages.at(index).len;
        }
        _messages.remove(index);
    }

    QDataStream& Stream(){ return _stream; }

private:
    QMutex _mutex;
    QWaitCondition _notEmpty;
    QWaitCondition _notFull;
    QByteArray _data;
    QVector<Message> _messages;
    int _maxSize;
protected:
    QDataStream _stream;

//signals:
//    void Process(QDataStream& stream);
};



///
/// \brief The Integrant class
///
class Integrant : public QObject
{
Q_OBJECT
public:
    Integrant(QString name, int sign, int length = 3);

    QString Name() const {return _name;}
    int Sign() const {return _sign;}
    int Length() const {return _length;}

private:
    QString _name;
    int _sign;
    int _length;

public slots:
    bool Token(QByteArray& data);
    void Do(QDataStream& data);
};



///
/// \brief The Unit class
///
//class Unit : public Queue
class Unit : public QObject
{
    Q_OBJECT
public:
    Unit(int chunkSize = 0);
    void Add(Integrant* i);
    void Job(QByteArray& data);        

signals:
    void Push(QByteArray data); //source thread
    void Do(QDataStream& data); //
    void UnknownMessageNotify(QByteArray& data);
private:
    QVector<Integrant*> _integrants;
    Queue _queue;
    int _chunkSize;
public slots:
    void Processing(QDataStream &stream);
    void ProcessingData(QByteArray data);
};


#endif // UNIT

